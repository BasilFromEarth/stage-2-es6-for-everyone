import { showModal } from './modal';
import App from './../../app';

export function showWinnerModal(fighter) {
  // call showModal function 
  console.log(fighter.name);
  showModal({title: 'Winner is', bodyElement: fighter.name});
}
